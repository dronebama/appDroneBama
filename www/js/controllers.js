angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);
    };
})

.controller('PlaylistsCtrl', function($scope) {
    $scope.playlists = [{
        title: 'Request Help',
        id: 1
    }, {
        title: 'Send Help',
        id: 2
    }];
})

.controller('PlaylistCtrl', function($scope, $stateParams, $http, $cordovaGeolocation, $ionicLoading) {

    $scope.type = $stateParams.playlistId;
    var serverUrl = 'http://192.168.2.100:3000';
    // var serverUrl = "https://www.google.co.in/maps/place/";
    // https://www.google.co.in/maps/place/52.9720011,5.9118825

    $scope.status = "";
    $scope.enabled = false;
    $scope.apiStatus = "";

    if ($scope.type == 1) {
        $scope.apiStatus = "Request Help";
    } else {
        $scope.apiStatus = "Send Help";
    }

    $scope.request = function(type) {
        // $scope.showImage = false;
        $scope.apiStatus = "sending request...";
        $scope.getHelp = function() {
            $http({
                method: 'GET',
                url: serverUrl + '/takeoff'
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                $scope.apiStatus = "Request Received!";
                // $scope.showImage = true;
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $scope.apiStatus = "Request failed!";
                // console.log(response)
            });
        };

        $scope.getHelp();

        var posOptions = {
            timeout: 10000,
            enableHighAccuracy: true
        };


        // onSuccess Callback
        // This method accepts a Position object, which contains the
        // current GPS coordinates
        //
        // var onSuccess = function(position) {
        //     $scope.status = "success";
        //     $scope.apiStatus = "Request Sent";
        //     var serverUrl = 'http://localhost:3000'
        //         // $http.get(serverUrl + '/sendCoordinodates?latitude=' + position.coords.latitude + '&longitude=' + position.coords.longitude).then(function(response) {
        //         //     $scope.apiStatus = "Request Received!" + response;
        //         // });

        //     $http({
        //         method: 'GET',
        //         url: serverUrl + '/sendCoordinodates?latitude=' + position.coords.latitude + '&longitude=' + position.coords.longitude
        //     }).then(function successCallback(response) {
        //         // this callback will be called asynchronously
        //         // when the response is available
        //         $scope.apiStatus = "Request Received!" + response;
        //     }, function errorCallback(response) {
        //         // called asynchronously if an error occurs
        //         // or server returns response with an error status.
        //         $scope.apiStatus = "Request failed!" + response;
        //         console.log(response)
        //     });


            // alert('Latitude: ' + position.coords.latitude + '\n' +
            //     'Longitude: ' + position.coords.longitude + '\n' +
            //     'Altitude: ' + position.coords.altitude + '\n' +
            //     'Accuracy: ' + position.coords.accuracy + '\n' +
            //     'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
            //     'Heading: ' + position.coords.heading + '\n' +
            //     'Speed: ' + position.coords.speed + '\n' +
            //     'Timestamp: ' + position.timestamp + '\n');
        // };

        // onError Callback receives a PositionError object
        //
        // function onError(error) {
        //     alert('code: ' + error.code + '\n' +
        //         'message: ' + error.message + '\n');
        // }

        // navigator.geolocation.getCurrentPosition(onSuccess, onError);


        // $cordovaGeolocation
        //   .getCurrentPosition(posOptions)
        //   .then(function (position) {
        //
        //     $scope.enabled = true;
        //
        //     var lat  = position.coords.latitude;
        //     var long = position.coords.longitude;
        //
        //     $http.get(api+lat+","+long)
        //     .then(function(response) {
        //         $scope.status = "success";
        //         $scope.apiStatus = "Request Sent";
        //         var serverUrl = '128.0.0.1:3000'
        //         $http.get(serverUrl + '/sendCoordinates?latitude=' + lat + '&longitude=' + long).then(function(response) {
        //             $scope.apiStatus = "Request Received!";
        //         });
        //     });
        //
        //   }, function(err) {
        //     // error
        //   });



    }

});